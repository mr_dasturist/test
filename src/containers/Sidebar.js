import React, {Component} from 'react';
import '../index.css'

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 0,
            data: {name: "React_JS_ALI"},
        }
    }

    clickHref = () => {
        let son = 45;
        let num = this.state.number;
        if (num === son) {
            alert('You have not Decrement')
        } else {
            this.setState({number: num + 1});
        }

    }
    clickDecrement = () => {
        let son = 0;
        let num = this.state.number;
        if (num === son) {
            alert('You have not Decrement')
        } else {
            this.setState({number: num - 1});
        }
    }
    clickClear = () => {
        this.setState({number: 0});
    }

    render() {
        const {number} = this.state;
        return (
            <div className="text">
                <h1>React js is now </h1>
                <hr/>
                <table cellPadding="0" cellSpacing="0">
                    <tbody>
                    <tr>
                        <td>
                            <a href="#" onClick={this.clickHref}>React ++ </a> <br/> <br/>
                            <a href="#" onClick={this.clickDecrement}>React - - </a> <br/><br/>
                            <a href="#" onClick={this.clickClear}>Clear</a>
                        </td>
                        <td>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            NUMBER: <span className="number">{number}</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Sidebar;