import React, {Component} from 'react';
import UserList from "../../components/UserList";
const list = {
    email: '',
    password: ''
}

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = list;
    }
    changeHandler = e =>{
        const {name, value} = e.target;
        this.setState({[name]:value});
    }
    render() {
        const {email, password} = this.state
        return (
            <div>
                <div>
                    <h1>Sing in</h1>
                    <form action="" onSubmit={this.formHandler}>
                        <label htmlFor="name">Email:</label><br/>
                        <input
                            type="email"
                            name="email"
                            placeholder="Your email"
                            value={email}
                            onChange={this.changeHandler}
                        />
                        <br/> <br/>
                        <label htmlFor="name">Password:</label><br/>
                        <input
                            type="password"
                            name="password"
                            placeholder="Your password"
                            value={password}
                            onChange={this.changeHandler}
                        /><br/><br/>
                        <button type='submit'>OK</button>
                    </form>
                </div>
                <div>
                    {
                        <UserList/>
                    }
                </div>
            </div>
        );
    }
}

export default SignIn;