import React, {Component} from 'react';

const url = "https://reqres.in/api/users?page=";

class UserList extends Component {
    state = {users: []}

    componentDidMount() {
        this.getUsersFromList();
    }

    getUsersFromList = (type) => {
        const url_page = url + type;
        fetch(url_page)
            .then(response => response.json())
            .then(data => this.setState({users: data.data}))
            .catch(err => console.log(err))
    }


    render() {
        const {users} = this.state;
        return (
            <div>
                <h1>This is Users</h1>
                <ul>
                    <table>
                        <tbody>
                        {
                            users.map(item => {
                                return (
                                    <tr key={item.id}>
                                        <td><img src={item.avatar} alt="item.first_name"/></td>
                                        <td>
                                            <b>{item.email}</b>
                                            <br/>
                                            {item.first_name + ' ' + item.last_name}</td>
                                    </tr>
                                )
                            })
                        }

                        </tbody>
                    </table>
                    <button onClick={() => this.getUsersFromList(1)}>1</button>
                    <button onClick={() => this.getUsersFromList(2)}>2</button>
                </ul>
            </div>
        );
    }
}

export default UserList;